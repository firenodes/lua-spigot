package com.firenodes.luaspigot.bukkit.command;

import com.firenodes.luaspigot.LuaSpigotBukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class LuaSpigotCommand implements CommandExecutor {

    // This method is called, when somebody uses our command
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            LuaSpigotBukkit.getInstance().reloadConfig();
            LuaSpigotBukkit.getInstance().reloadAddons();
            sender.sendMessage(ChatColor.GREEN + "Reloaded LuaSpigot Config.");
        } catch (Exception ex) {
            sender.sendMessage(ChatColor.RED + ex.toString());
        }
        return true;
    }
}