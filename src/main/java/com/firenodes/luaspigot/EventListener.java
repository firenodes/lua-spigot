package com.firenodes.luaspigot;

import lombok.RequiredArgsConstructor;
import com.firenodes.luaspigot.lua.lib.EventLibrary;
import org.bukkit.event.Listener;

@RequiredArgsConstructor
public abstract class EventListener implements Listener {
    protected final EventLibrary lib;
}