package com.firenodes.luaspigot;

import com.firenodes.luaspigot.bukkit.command.LuaSpigotCommand;
import com.firenodes.luaspigot.lua.State;
import com.firenodes.luaspigot.lua.lib.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class LuaSpigotBukkit extends JavaPlugin {
    private LuaSpigot api;
    private final File addonFolder = new File(this.getDataFolder(), "addons");
    private State globalState;

    @Override
    public void onLoad() {
        api = new LuaSpigot(this);
        this.saveDefaultConfig();
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    @Override
    public void onEnable() {
        Objects.requireNonNull(this.getCommand("luaspigot")).setExecutor(new LuaSpigotCommand());
        getLogger().info("LuaSpigot successfully loaded! You may now use its" +
                " functionality in your plugins.");
    }

    public LuaSpigot getAPI() {
        return this.api;
    }

    public State getGlobalState() {
        return this.globalState;
    }

    public static LuaSpigotBukkit getInstance() {
        return getPlugin(LuaSpigotBukkit.class);
    }

    protected void registerBuiltInLibraries() {
        if (this.getConfig().getBoolean("loadDefaultLibraries")) {
            this.getAPI().getLibraryRegistry().register(new BukkitUtilsLibrary());
            this.getAPI().getLibraryRegistry().register(new DatabaseLibrary());
            this.getAPI().getLibraryRegistry().register(new ConfigLibrary());
            this.getAPI().getLibraryRegistry().register(new CommandLibrary());
            this.getAPI().getLibraryRegistry().register(new EventLibrary(this));
            this.getAPI().getLibraryRegistry().register(new InventoryLibrary(this));
            this.getAPI().getLibraryRegistry().register(new TaskLibrary(this));
        }
    }

    public void reloadAddons() {
        getLogger().info("Loading LuaSpigot addons...");
        this.registerBuiltInLibraries();
        this.globalState = this.getAPI().newState();
        this.globalState.execute("package.path = \"" + addonFolder.toPath().resolve("?.lua").toString() + "\"");
        if (!addonFolder.exists()) addonFolder.mkdirs();
        File[] listOfFiles = addonFolder.listFiles();

        for (int i = 0; i < Objects.requireNonNull(listOfFiles).length; i++)
            if (listOfFiles[i].isFile()) {
                try {
                    globalState.load(listOfFiles[i].getPath());
                } catch (IOException e) {
                    getLogger().warning(String.format("Could not load addon %s", listOfFiles[i].getPath()));
                    e.printStackTrace();
                }
            }
    }
}
