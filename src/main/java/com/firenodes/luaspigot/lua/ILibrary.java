package com.firenodes.luaspigot.lua;

/**
 * Interface defining mandatory methods for Lua libraries to override.
 */
interface ILibrary {
    String getName();
}
