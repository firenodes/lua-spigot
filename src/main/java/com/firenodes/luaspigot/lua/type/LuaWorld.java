package com.firenodes.luaspigot.lua.type;

import com.firenodes.luaspigot.lua.TypeUtils;
import com.firenodes.luaspigot.lua.WrapperType;
import com.firenodes.luaspigot.lua.annotation.DynamicFieldDefinition;
import com.firenodes.luaspigot.lua.annotation.MethodDefinition;
import com.firenodes.luaspigot.lua.type.util.LuaUUID;
import org.apache.commons.lang.enums.EnumUtils;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import java.util.Arrays;

/**
 * Wrapper type describing a Minecraft world.
 */
public class LuaWorld extends WrapperType<World> {
    private static final LuaValue typeMetatable = LuaValue.tableOf();

    public LuaWorld(World w) {
        super(w);
    }

    @Override
    public String getName() {
        return "world";
    }

    @Override
    public String toLuaString() {
        World w = getHandle();
        return "world: " + w.getName() + " (" + w.getUID().toString() + ")";
    }

    @DynamicFieldDefinition("value")
    public LuaValue getWName() {
        return LuaValue.valueOf(getHandle().getName());
    }

    @DynamicFieldDefinition("uuid")
    public LuaValue getUUID() {
        return new LuaUUID(getHandle().getUID());
    }

    @MethodDefinition("spawnEntity")
    public Varargs spawnEntity(Varargs args) {
        LuaString entityName = args.checkstring(1);
        TypeUtils.validate(args.checktable(2), "location");
        LuaLocation location = (LuaLocation) args.checktable(2);
        return new LuaEntity(this.getHandle().spawnEntity(
                location.getHandle(),
                Arrays.stream(EntityType.values()).filter(t -> t.getKey().toString().equals(entityName.tojstring()) || t.name().toLowerCase().equals(entityName.tojstring().toUpperCase().split(":")[1])).findFirst().orElse(EntityType.FALLING_BLOCK)
        ));
    }

    @Override
    protected LuaValue getMetatable() {
        return typeMetatable;
    }
}
